from django.urls import path
from . import views
from .views import *
from django.shortcuts import render

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('portofolio/', views.portofolio, name='portofolio'),
    path('formRespon/', views.formRespon, name='formRespon'),
    path('formJadwal/', views.formJadwal, name='formJadwal'),
    path('formUbah/<int:pk>', views.formUbah, name='formUbah'),
    path('formHapus/<int:pk>', views.formHapus, name='formHapus'),
]