from django.shortcuts import render, redirect
from .forms import FormJadwalKegiatan
from .models import JadwalKegiatan
import datetime

# Create your views here.
def base(request):
    return render(request, 'base.html')

def home(request):
    return render(request, 'Tugas3.html')

def portofolio(request):
    return render(request, 'Tugas3-Foto.html')

def formJadwal(request):
    form = FormJadwalKegiatan(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('homepage:formRespon')
        else:
            form = FormJadwalKegiatan()
            
    konten = {'title' : 'Form Jadwal Kegiatan', 'form' : form}
    return render(request, 'formulir.html', konten)

def formRespon(request):
    respon = JadwalKegiatan.objects.all()
    konten = {'title' : 'Jadwal Kegiatan', 'respon' : respon}
    return render(request, 'formulirRespon.html', konten)

def formUbah(request, pk):
    respon = JadwalKegiatan.objects.get(pk=pk)
    form = FormJadwalKegiatan(request.POST, instance=respon)
    if request.method == "POST":
        if form.is_valid():
            respon = form.save()
            return redirect('homepage:formRespon')
        else:
            form = FormJadwalKegiatan(instance=respon)

    konten = {'title' : 'Jadwal Kegiatan', 'form' : form, 'jawaban' : respon}
    return render(request, 'formulirUbah.html', konten)

def formHapus(request, pk):
    JadwalKegiatan.objects.filter(pk=pk).delete()
    respon = JadwalKegiatan.objects.all()
    konten = {'title' : 'Jadwal Kegiatan', 'respon' : respon}
    return redirect('homepage:formRespon')

