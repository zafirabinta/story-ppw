from django import forms
from .models import JadwalKegiatan
from django.forms import ModelForm

class FormJadwalKegiatan(ModelForm):
    class Meta:
        model = JadwalKegiatan
        fields = ['nama_kegiatan', 'hari', 'tanggal', 'waktu', 'tempat', 'kategori']
        labels = {'nama_kegiatan' : 'kegiatan apa?', 'hari' : 'hari apa?', 'tanggal' : 'tanggal berapa?', 'waktu' : 'jam berapa?', 'tempat' : 'di mana?', 'kategori' : 'acara apa?'}

        widgets = {
            'nama_kegiatan' : forms.TextInput(attrs={'class' : 'form-control', 'type' : 'text', 'placeholder' : 'Tidur'}),
            'hari' : forms.TextInput(attrs={'class' : 'form-control', 'type' : 'text', 'placeholder' : 'Hari ini'}),
            'tanggal' : forms.DateInput(format=('%d-%m-%Y'), attrs={'class' : 'form-control', 'type' : 'date'}),
            'waktu' : forms.TimeInput(format='%I:%M %p', attrs={'class' : 'form-control', 'type' : 'time'}),
            'tempat' : forms.TextInput(attrs={'class' : 'form-control', 'type' : 'text', 'placeholder' : 'Di rumah'}),
            'kategori' : forms.Select(attrs={'class' : 'form-control'})}