from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.

KATEGORI = [('Tugas', 'Tugas'),
            ('Ujian', 'Ujian'),
            ('Rapat', 'Rapat')]

class JadwalKegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=50)
    hari = models.CharField(max_length=50)
    tanggal = models.DateField()
    waktu = models.TimeField()
    tempat = models.CharField(max_length=50)
    kategori = models.CharField(max_length=50, choices=KATEGORI)
